Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: jpeginfo
Upstream-Contact: Timo Kokkonen <tjko@iki.fi>
Source: https://github.com/tjko/jpeginfo
Files-Excluded: test/*.jpg
Comment:
 These images are licensed under CC BY-NC-SA 4.0.
 The NC (non-commercial) part of the license is not DFSG-free.

Files: *
Copyright: 1995-2023 Timo Kokkonen <tjko@iki.fi>
License: GPL-3

Files: debian/*
Copyright: 1997-1999 Nicolás Lichtmaier <nick@feedback.net.ar>
           2002-2014 Ola Lundqvist <opal@debian.org>
           2021 Andreas Tille <tille@debian.org>
           2024 xiao sheng wen <atzlinux@sina.com>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation; either version 2, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details.
 .
 On Debian systems you can find a full copy of the GNU General Public
 License version 2 at /usr/share/common-licenses/GPL-2.

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
